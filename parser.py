import os
import sys, argparse, copy
from datetime import *
from mrtparse import *
import csv
import bgpdump
import os.path
import gzip
import shutil
from os import path

def clean(line):
    str=''
    finalstr="" 
    for i in line:
        str+=i+" "
    return(str[:-1])


def parsemrt(p,filename):
    print("parsing file: ", filename)
    args = bgpdump.parse_args()
    args.path_to_file = p+filename
    d = Reader(args.path_to_file)
    with open("../bgp_data/"+filename+".csv",'w',newline='') as file:
        writer  = csv.writer(file)
        #writer.writerow(["Prefix","Time Stamp","Aspath"])
        count = 0
        for m in d:
            if count == 1001:
                break
            m = m.mrt
            if m.err:
                continue
            b = bgpdump.BgpDump(args)
            if m.type == MRT_T['TABLE_DUMP_V2']:
                a = b.td_v2(m)
                asp=clean(a.as_path)
                prefix = clean(a.nlri)
                time = a.org_time
                if time != 0:
                        writer.writerow([time,str(prefix),str(asp)])
                        #print(prefix)
            count += 1
    #print("parsing done", filename)





def main():
    files = []
    path = "../bgp_download/rrc00/2020/04/"
    for i in os.listdir(path):
        if i.endswith('.gz'):
            file = i
            if file+".csv" in os.listdir(path):
                print("File already parsed:", i)

            else:
                #print("File to be parsed:", i)
                parsemrt(path,i)





if __name__ == '__main__':
    main()


    
        
